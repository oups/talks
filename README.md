# Talks

A list of the past talks at OUPS, with accompanying materials

## 09/12/21

Videos are available at [https://www.irill.org/videos/OUPS/2021-12/](https://www.irill.org/videos/OUPS/2021-12/)

* François Pottier, "Monolith", [slides](12_21/pottier.pdf)
* François Bérenger, "Générer 340000 molécules valides par seconde sur un seul cœur et à dos de chameau", [slides](12_21/berenger.pdf)
* Pierre Chambart, "Pourquoi écrire du C quand on peut faire pire en OCaml ?", [slides](12_21/chambart.pdf)

## 18/03/22

* Timothy Bourke, "SundialsML", [slides](03_22/bourke.pdf), [library](http://inria-parkas.github.io/sundialsml/), [example](https://github.com/inria-parkas/sundialsml/blob/24d1e026622ca0a3c1c82aee15424fd4ed49e4f8/examples/ocaml/dpendulum/dpendulum.ml)
* Benjamin Nguyen, "Epidemiological inference in OCaml", [slides](03_22/nguyen.pdf)
