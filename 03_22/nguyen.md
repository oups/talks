Epidemiological inference in OCaml

OCaml is a great choice for simulating mathematical models: Its functional
nature and its type and module system allow to easily express mathematical models
and to simulate them in a modular and (reasonably) efficient manner.
I will share some of the [code](https://gitlab.com/bnguyenvanyen/ocamlecoevo)
and results from my PhD about the Bayesian inference of infectious diseases,
with simulation of stochastic processes, MCMC, phylogenetic inference, and musical populations.
