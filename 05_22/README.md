# Meetup du 12 mai 2022

## Sponsor

![Logo LambdaNantes](LambdaNantes.png)

LambdaNantes est une collection d'évènements (récurrents) qui s'intéressent à l'utilisation des langages "dit Applicatifs" (ou fonctionnels) à Nantes.
